## CASE STUDY: We are planning to build a backend for an e-commercial platform, your task is doing from system design, implementation to deploy production.

### SECTION I:

1. Design ERD for the backend of these features based on best practices to ensure scalable, easy coding. (Complete)

[Link pdf ERD](ERD_4.pdf)

### SECTION II:

1. With ERD you have already designed on question 1, what database are you using to implement? (Complete)

I using MongoDB to implement

2. Why are you using that? What is the strong and weak point of it? (Complete)

I using MongoDB because MongoDb have strong point:

- Easy to horizontal scalability
- Easy setup
- Stores data in the RAM, queries quickly
- Performs fast and high performance
- Stores documents with JSON

**Weak point:**

- High memory usage
- Relation not equal RDBMS
- Duplicate data

3. Write docker-compose.yml to start the database locally. (Complete)

```sh
  docker-compose up -d
```

4. Setup Loopback 4 (Complete)

5. Using UML on question 1, set up API for these features. (Complete)

### SECTION III:

1. Write a sequence diagram to build a solution for authentication and
   authorize adapt the list of features below. (Complete)

[Download file svg Auth Sequence Diagram](Auth_Sequence_Diagram.svg)

2. Using solution on question 1, implements loopback4 for these features. (Incomplete)
3. What are the strong and weak points of your solution? How to improve
   that? (Incomplete)
4. Build a solution for testing, ensure correct permission scalable from 100
   APIs to 1000 APIs. (Incomplete)

### SECTION IV: Good job, right now our application needs to synchronize products, pricing of the Agency by using third-party API.

1. Write a sequence diagram to build a solution to save, merge products data
   from third-party API to our database. (Third-party API data change every hour) (Incomplete)
2. What are the strong and weak points of your solution? How to improve
   that? (Incomplete)

### SECTION V:

1. Write an architecture diagram to build a solution adapt the list of
   features below. (Incomplete)

- Ensure isolating development and production data. (Don't merge data
  together)
- Apply Gitlab CI or Github Action to test, build and deploy to Heroku
  automatically.
